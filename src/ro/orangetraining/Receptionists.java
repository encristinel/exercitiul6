package ro.orangetraining;

public class Receptionists extends EmployeeSalaries {

    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Receptionists(int salary ){
        this.salary = salary;
    }

    @Override
    public void CalculateSalaries() {
        System.out.println("The salary of Receptionists is somewhere around " + salary);
    }
}